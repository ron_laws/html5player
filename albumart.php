<?php
//Somewhat glitchy Google based album art fetcher.. plan to use a more reliable one in the future

require_once('./config/config.php');	 //Include Config
include('./lib/imageResize.php'); 		//Include image resize lib
      
function getNowPlaying($sc_url_ip,$sc_url_port) //Quiery shoutcast server for currently playing track
{
$fp = @fsockopen($sc_url_ip,$sc_url_port,$errno,$errstr,1);
if (!$fp) { 
    $title = ""; //print nothing, it should trigger the fallback for the album-art fetch
} else { 
    fputs($fp, "GET /7.html HTTP/1.0\r\nUser-Agent: Mozilla\r\n\r\n");
    while (!feof($fp)) {
        $info = fgets($fp);
    }
    $info = str_replace('</body></html>', "", $info);
    $split = explode(',', $info);
    if (empty($split[6])) {
        $title = ""; //print nothing, it should trigger the fallback for the album-art fetch
    } else {
        $count = count($split);
        $i = "6";
        while($i<=$count) {
            if ($i > 6) {
                $title .= ", " . $split[$i];
            } else {
                $title .= $split[$i];
            }
            $i++;
        }
    }
}
$title = substr($title, 0, -2);  
return $title;    
}

function getNewAlbumArt($current_song, $cacheDIR) //Download album art to cache
{
	$search = urlencode($current_song);
	$jsrc = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=$search";
	$json = file_get_contents($jsrc);
	$jset = json_decode($json, true);
	$imgurl = $jset["responseData"]["results"][0]["url"];
	$imgurl = reset(explode('%', $imgurl));
	$in = fopen($imgurl, "rb");
    $out = fopen("./$cacheDIR/artwork_original.jpg", "wb");
    while ($chunk = fread($in,8192))
    {
        fwrite($out, $chunk, 8192);
    }
    fclose($in);
    fclose($out);
    if ( 0 == filesize("./$cacheDIR/artwork_original.jpg") )
	{
		// file is empty, or failed to download
		copy("./css/audio.jpg", $cacheDIR."/artwork_original.jpg"); //Copy a generic audio image as a last resort
	}
}


function cacheWriteNowPlaying($current_song, $cacheDIR) //write now playing to the cache
{
	$file = fopen($cacheDIR."/nowplaying.txt","w") or die(print "Could not open nowplaying.txt");
	fwrite($file,$current_song);
	fclose($file);
}

// Headless Cache with checksum output
$class = $_GET['c'];
if ($class == "cache") {
	if ($current_song==$cachedNowPlaying) {
		echo(md5_file($cacheDIR."/artwork.jpg"));
		exit;
		} else {
		$lock = fopen($cacheDIR.'/lock.txt', 'r+');
		/* Activate the LOCK_NB option on an LOCK_EX operation */
		if(!flock($lock, LOCK_EX | LOCK_NB)) {
			echo 'Unable to obtain lock';
			exit(-1);
		}
		cacheWriteNowPlaying($current_song,$cacheDIR);
		getNewAlbumArt($current_song,$cacheDIR);
		$image = new SimpleImage(); 
		$image->load($cacheDIR."/artwork_original.jpg"); 
		$image->resizeToWidth(640);				//resize image to 640px
		$image->save($cacheDIR."/artwork.jpg");
		fclose($lock); 
		echo(md5_file($cacheDIR."/artwork.jpg"));
		exit;
	}
}
// Checksum only
$class = $_GET['c'];
if ($class == "checksum") {
		echo(md5_file("./cache/artwork.jpg"));
		exit;
}


$raw_title = getNowPlaying($sc_url_ip,$sc_url_port);				//Get whats playing
$current_song = strip_tags($raw_title, '</body></html>'); 			//remove the html tags at the end (just in case)
$cachedNowPlaying = file_get_contents("$cacheDIR/nowplaying.txt"); 	//Fetch last known title from cache

if ($current_song==$cachedNowPlaying) 								//Compare the two
{
	//Nothing changed, dont need to do anything but return the currently cached image
	echo('<img class="'.$class.'" src="'.$cacheDIR.'/artwork.jpg" />');  	//Spit out the image
	
} else {
	$lock = fopen($cacheDIR.'/lock.txt', 'r+');
	/* Activate the LOCK_NB option on an LOCK_EX operation */
	if(!flock($lock, LOCK_EX | LOCK_NB)) {
		echo 'Unable to obtain lock';
		exit(-1);
	}
	//Whats playing changed since we last checked, so we need to update our cache.
	cacheWriteNowPlaying($current_song,$cacheDIR);
	getNewAlbumArt($current_song,$cacheDIR);
	$image = new SimpleImage(); 
	$image->load($cacheDIR."/artwork_original.jpg"); 
	$image->resizeToWidth(800);				//resize image to 640px
	$image->save($cacheDIR."/artwork.jpg");
	fclose($lock);
	echo('<img class="'.$class.'" src="'.$cacheDIR.'/artwork.jpg" />');		//Spit out the image
}

?>
