<?php
require_once('./config/config.php'); //Include Config

function getNowPlaying($sc_url_ip,$sc_url_port)
{
$fp = @fsockopen($sc_url_ip,$sc_url_port,$errno,$errstr,1);
if (!$fp) { 
    $title = "Connection timed out or the server is offline  ";
} else { 
    fputs($fp, "GET /7.html HTTP/1.0\r\nUser-Agent: Mozilla\r\n\r\n");
    while (!feof($fp)) {
        $info = fgets($fp);
    }
    $info = str_replace('</body></html>', "", $info);
    $split = explode(',', $info);
    if (empty($split[6])) {
        $title = "The current song is not available  ";
    } else {
        $count = count($split);
        $i = "6";
        while($i<=$count) {
            if ($i > 6) {
                $title .= ", " . $split[$i];
            } else {
                $title .= $split[$i];
            }
            $i++;
        }
    }
}
$title = substr($title, 0, -2);  
return $title;
}

//////////////////

//get the now playing

$current_song = getNowPlaying($sc_url_ip,$sc_url_port);
//this code replaces the - in titles with a <br> element, to make artist and title appear
// on seperate lines, however, its very hacky and not that reliable, and can cause issues...
//
//$order   = array("-");
//$replace = '<br />';
//$newstr = str_replace($order, $replace, $current_song);

if ($_GET['hash']) {
 echo(md5($current_song));
 exit;
}

//print $newstr; //part of the hacky code above
echo(htmlspecialchars($current_song)); //print song and make sure it's html friendly

?>
