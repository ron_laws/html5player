<?php

// Shoutcast Server location
$sc_url_ip 		= "stream.project-yazu.co.uk";	//Hostname or IP only, 
$sc_url_port 	= "8000"; 						//Shoutcast port


$cacheDIR 		= "./cache";	//It's best to leave this in the relative directory of the player, you can change it if you wish.
								//If you choose to change it, make sure it's somewhere the file can be accessed by clients.
// You will need a Gracenote Client ID to use this. Visit https://developer.gracenote.com/user/register
// for more information.

//$clientID  = "1736960-C6C9B6572906FF7FEABF275403C48A9B"; // Put your Client ID here.
//$clientTag = "C6C9B6572906FF7FEABF275403C48A9B"; // Put your Client Tag here.


// Debug Settings
error_reporting(1);  			//Report errors
//ini_set('display_errors', 0); 	//Leave this disabled unless you want to unleash all kinds of hell

?>
