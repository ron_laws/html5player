<?php require_once('./config/config.php'); //Include Config ?>
<!doctype html>
<html lang="en-GB">
<head>
  <meta charset="utf-8">
  <title>HTML5 Audio Player</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
	<style type="text/css">
		<?php
		if(preg_match("/(?i)msie [1-9]\.0/",$_SERVER['HTTP_USER_AGENT']))
		{
			print("
			#ie-warning {
			padding: 10px;
			width: 100%;
			size: 20px;
			z-index: 1000;
			position: fixed;
			top: 25%;
			color: white;
			background: #693737;
			}
			#ie-warning h1 {
				color: red;
			}
			#ie-warning p {
				color: pink;
				text-align: center;
			}");
			
		} else {
			print("
			#ie-warning {
			display: none;
			width: 0;
			height: 0;
			}	
				
			");
			echo("/* ".$_SERVER['HTTP_USER_AGENT']." */");
			}
			?> 
			
	
	</style>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
  <script type="text/javascript" src="js/mediaelement-and-player.js"></script>
	<script>
			function update() {
							$('.album-art1').load('albumart.php?c=album-art',function(){
							$('.album-art').fadeOut('100',function(){
									d = new Date();
									$('.album-art1').load('albumart.php?c=album-art');
									$(".album-art").attr("src", "<?php echo("$cacheDIR");?>/artwork.jpg");
									$('.album-art').fadeIn('1000');
							});
							});
							$('.album-art-back1').load('albumart.php?c=album-art-back',function(){
							$('.album-art-back').fadeOut('100',function(){
										d = new Date();
										$('.album-art-back1').load('albumart.php?c=album-art-back');
										$(".album-art-back").attr("src", "<?php echo("$cacheDIR");?>/artwork.jpg");
										$('.album-art-back').fadeIn('1000');
							});
							});
			};
			function artcheck() {
			$.get('albumart.php?c=album-art',function(data){	
				var $check = (data);
				var $check2 = '<img class="album-art" src="./cache/artwork.jpg" />';
					if ($check == $check2) {

						update();

					}
					else {

						startRefresh();

					}
			});
			}
			$(function() {
			startRefresh();
			});
			function startRefresh() {
				setTimeout(startRefresh,15000);
				$.get("stream.php",function(data){
					var $check = (data);
					var $title = $(".music-title").html();
					
						if ($check != $title) {
						
							$('.music-title').load('stream.php');
								
							artcheck();	
						};
				});
			};
		  </script>
			<style type="text/css">
			/* CSS for the blur effect in Firefox, required to be in the HTML due to limitations in SVG */
			@-moz-document url-prefix() {
				.album-art-back {
				 /*opacity:0.6;*/
				-moz-filter:blur(5px);
				filter:url('#svgBlur');
				filter: blur(5px);
				}
			}
			</style>
</head>
<body> 
	
	<div id="ie-warning"><h1>Internet Explorer </h1>
	<p>Internet Explorer does not currently support the most fundimental and basic of modern web standards in HTML5/CSS3, and as such, this player does not work in IE at this time. ;(</p>
	<p>You could always give other browsers a try, like <a href="https://www.mozilla.org">Firefox</a> or <a href="http://www.google.com/chrome">Google Chrome,</a> you may actually find the internet looks alot less broken! ;)</p>
	</div>
	
  <div id="wrapper">
    <div id="content">
      <div class="audio-player">
        <h2 class="music-title">
		<?php include('./stream.php'); ?>
		</h2>
		<div class="album-art-container" style="">
		 <svg height="0" xmlns="http://www.w3.org/2000/svg">
		  <filter id="svgBlur" x="-5%" y="-5%" width="110%" height="110%">
			<feGaussianBlur stdDeviation="5" />
			<feColorMatrix type="saturate" values="0.4" />
				<div class="album-art-back1"></div>
		  </filter>
		 <div class="album-art1"></div>
		</div>
        <audio id="audio-player"><source src="http://<?php echo("$sc_url_ip:$sc_url_port");?>/;" type="audio/mpeg"></audio>
      </div><!--end audio-player -->
    </div><!-- end content -->
  </div><!-- end wrapper -->
<script type="text/javascript">
$(function(){
  $('#audio-player').mediaelementplayer({
    alwaysShowControls: true,
    features: ['playpause','volume'],
    audioVolume: 'horizontal',
    audioWidth: '100%',
    audioHeight: '100%',
    startVolume: '0.5',
    iPadUseNativeControls: false,
    iPhoneUseNativeControls: false,
    AndroidUseNativeControls: false
  });
});
</script>
</body>
</html>
